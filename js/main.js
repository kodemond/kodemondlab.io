// setup
const fontArms = [".d1", ".d4", ".c2", ".c4", ".o2", ".o3", ".u2", ".c8", ".c9", ".s1", ".s5", ".s2", ".t1", ".r1", ".u3", ".c10", ".c6", ".t3"];
const fontRight = [".d3", ".c1", ".c5", ".o4", ".n3", ".s4", ".r4", ".u2", ".c8", ".c9"];
const fontLeft = [".d2", ".c3", ".o1", ".n1", ".s3", ".r2", ".u1", ".c7"]

// generate some glitchy keyframes
function getGlitchy(
  glitchKeyframeAmount,
  glitchKeyframeOffset,
  glitchFrameSize
) {
  const keyframes = [];
  for (i = 0; i < glitchKeyframeAmount; i++) {
    const keyframe = {};
    const randomNumber1 = gsap.utils.random(10, 90, 5);
    const randomNumber2 =
      randomNumber1 + gsap.utils.random(-glitchFrameSize, glitchFrameSize, 2);
    const randomOffset = gsap.utils.random(
      glitchKeyframeOffset,
      -1 * glitchKeyframeOffset
    );
    const isLastFrame = i === glitchKeyframeAmount - 1;

    if (i % 3 === 0 || isLastFrame) {
      keyframe.clipPath = `polygon(0 ${randomNumber1}%, 100% ${randomNumber1}%, 100% ${randomNumber1}%, 0 ${randomNumber1}%)`;
    } else {
      keyframe.clipPath = `polygon(0 ${randomNumber1}%, 100% ${randomNumber1}%, 100% ${randomNumber2}%, 0   ${randomNumber2}%)`;
      keyframe.xPercent = randomOffset;
    }
    keyframe.webkitClipPath = keyframe.clipPath;
    keyframes.push(keyframe);
  }
  return keyframes;
};

// nice flickery eases, they need easePack 
const flicker = "rough({ template: sine.inOut, strength: 1.5, points: 10})";
const flicker2 = "rough({template: power0.none, strength: 2, points: 12})";
const strongFlicker = "rough({template: power2.inOut, strength: 2.5, points: 15})";

const glitchKeyframes = getGlitchy(8, 2, 20);

// timelines
function randomizedStretching() {
  var timeline = gsap.timeline()
  .from(".letters", {
    duration: 1,
    ease: flicker2,
    xPercent: (i) => {
			return gsap.utils.random(-400, 400, 5);
		},
    scaleX: (i) => {
      return gsap.utils.random(0, 10);
    },
    transformOrigin: "50%, 50%",
    stagger: 0.05
  },0)

  return timeline;
}
function opacityFlicker() {
  var timeline = gsap.timeline({defaults: {duration: 1.5,opacity: 0,stagger: 0.1}})
  .from(".letter--one", {
    ease: strongFlicker,   
  },0)
  .from(".letter--two", {
    ease: flicker2,
  },0)
  
  return timeline;
}
function linesTimeline() {
 var timeline = gsap.timeline({defaults:{ease: "expo.out", duration: 1}})
.from(fontLeft, {
  xPercent: 100,
},0.4)
.from(fontRight, {
  xPercent: -100,
},0.4)
.from(fontArms, {
  scaleX: 0,
  transformOrigin:"50% 50%",
},0)
.from(".n2", {
  rotate: 20,
  scale: 0.9,
  transformOrigin:"50% 50%"
},0)
.from(".r3", {
  rotate: 19,
  xPercent: -30,
  opacity: 0,
  transformOrigin:"50% 50%"
},0)
 return timeline;
}
function openingPinkLines() {
  var timeline = gsap.timeline({defaults:{ duration: 0.5,}})
  .fromTo(".dash",{
    scaleY: 0,
    scaleX: 0,
    xPercent: -200,
    transformOrigin: "50%,50%",
    ease: "linear",
  },{
    scaleY: 0.2,
    scaleX: 20,
    ease: "linear",
  },)
  .from(".dot",{
    xPercent: 900,
    scaleX: 300,
    transformOrigin: "100%,100%",
    ease: strongFlicker,
    opacity: 0,
  })
  .to(".dash",{
    scaleX: 1,
    scaleY: 1,
    xPercent: -950,
    transformOrigin: "100%,100%",
    ease: strongFlicker,
  }, "-=0.5")
  return timeline;
}
function glitch() {
  var timeline = gsap.timeline()
  .to(".glitchLayer",{
    duration: 0.1,
    opacity: 1,
  },0)
  .to(".glitchLayer",{
    duration: 1.5,
    ease: "flicker2",
    keyframes: glitchKeyframes
  },0)
  return timeline;
}
function textGlitch() {
  var timeline = gsap.timeline({defaults: { duration: 0.5, transformOrigin: "100%,100%", ease: flicker, }})
   .to(".c5",{
    xPercent: 80,
   },0)
  .to(".d1",{
    xPercent: 28,
  },0)
  .to(".s5",{
    xPercent: -30,
  },0)
  .to(".n1",{
    yPercent: 20,
  },0)
  .to(".n3",{
    yPercent: -20,
  },0)
   .to(".c6",{
    yPercent: -90,
  },0)
  .to(".u3",{
    yPercent: 90,
  },0)
  .to(".r3",{
    yPercent: 30,
  },0)
  .to( ".t4",{
    yPercent: 30,
  },0)
  return timeline;
}
function finalTextGlitch() {
  var timeline = gsap.timeline()
  .to([".d1", ".n1", ".n3", ".s5", ".r3", ".t4"],{
    duration: 0.2,
    yPercent: 0,
    xPercent: 0,
    transformOrigin: "100%,100%",
    ease: flicker,
  })
  return timeline;
}

// main timeline
var masterTimeline = gsap.timeline({delay:1.5})
.add(openingPinkLines(),0)
.add(opacityFlicker(),0.5)
.add(randomizedStretching(),0.7)
.add(linesTimeline(),2.8)
.add(glitch(),"-=0.5")
.add(textGlitch(),"-=1")
.add(glitch().timeScale(1.1),"+=1")
.add(finalTextGlitch().timeScale(1.1),"-=0.5")


//Chris Gannons timeline scrubber tool
// ScrubGSAPTimeline(masterTimeline);